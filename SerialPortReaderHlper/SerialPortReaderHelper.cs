﻿using System;
using System.IO.Ports;
using System.Runtime.CompilerServices;

namespace SerialPortReaderHlper
{
    public class SerialPortReaderHelper
    {
        private SerialPort port;
        private bool correct;

        public SerialPortReaderHelper()
        {
            correct = false;
            var availablePorts = SerialPort.GetPortNames();
            if (availablePorts.Length == 0)
            {
                Console.WriteLine("No available serial ports");
                return;
            }
            Console.Out.WriteLine("Choose needed serial port");
            Console.WriteLine("0 - exit");
            for (int i = 0; i < availablePorts.Length; ++i)
            {
                Console.WriteLine((i + 1) + " - " + availablePorts[i]);
            }
            var chosen = Convert.ToInt32(Console.ReadLine());
            if (chosen == 0)
            {
                Console.WriteLine("No one port is chosen, creating nullable serial port\n" +
                                  "Restart program to rechoose serial port or choose another read method");
                return;
            }
            port = new SerialPort();
            try
            {
                // настройки порта
                port.PortName = availablePorts[chosen - 1];
                port.BaudRate = 256000;
                port.DataBits = 8;
                port.Parity = System.IO.Ports.Parity.None;
                port.StopBits = System.IO.Ports.StopBits.One;
                port.ReadTimeout = 10;
                port.WriteTimeout = 1000;
                port.Open();
            } catch(Exception e) {
                Console.WriteLine ("ERROR: невозможно открыть порт:" + e.ToString ());
                return;
            }
            
            // _serialPort.DataReceived += new SerialDataReceivedEventHandler(_serialPort_DataReceived);
            // _serialPort.Open();
            correct = true;
        }

        private void _serialPort_DataReceived(object sender,
            SerialDataReceivedEventArgs e)
        {
            // Show all the incoming data in the port's buffer
            Console.WriteLine(port.ReadExisting());
        }
        
        public string GetLine()
        {
            if (Empty()) return null;
            try
            {
                var line = port.ReadLine();
                return line;
            }
            catch (TimeoutException)
            {
                return null;
            }
        }
        
        public bool Empty()
        {
            return !correct || !port.IsOpen;
        }

        ~SerialPortReaderHelper()
        {
            if (correct) port.Close();
        }
    }
}