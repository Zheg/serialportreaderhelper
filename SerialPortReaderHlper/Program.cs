﻿using System;
using System.IO;

namespace SerialPortReaderHlper
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            SerialPortReaderHelper reader = new SerialPortReaderHelper();
            using (var sw = new StreamWriter("LOG.txt"))
            {
                var stop = "";
                //Console.WriteLine("Input  0  to stop the code");
                sw.WriteLine("Program started at " + DateTime.Now.ToString("h:mm:ss tt"));
                int count = 0;
                while (count < 1000 && !reader.Empty())
                {
                    ++count;
                    if (!reader.Empty())
                        sw.WriteLine(reader.GetLine());
                    //stop = Console.ReadLine();
                }
                sw.WriteLine("Program finished at " + DateTime.Now.ToString("h:mm:ss tt"));
            }
        }
    }
}